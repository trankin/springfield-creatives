<li class="person-item">
    <a href="<?php echo $link; ?>">
        <img src="<?php echo $user_image; ?>" alt="<?php echo $name; ?>" />
    </a>
    <h3><a href="<?php echo $link; ?>"><?php echo $name; ?></a></h3>
    <h4><?php echo $subtitle ?></h4>
    <a href="mailto:<?php echo $email; ?>" class="email">Email</a>
</li>
